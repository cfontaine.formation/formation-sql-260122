DROP DATABASE IF EXISTS pizzeria;

CREATE DATABASE pizzeria;

USE pizzeria;

CREATE TABLE pizzas(
	numero_pizza INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL ,
	base VARCHAR(20) NOT NULL DEFAULT 'rouge',
 	prix DOUBLE NOT NULL DEFAULT 12.0,
 	photo MEDIUMBLOB
);

CREATE TABLE ingredients(
	numero_ingredient INT PRIMARY KEY AUTO_INCREMENT ,
	nom VARCHAR(40) NOT NULL DEFAULT ''
); 

CREATE TABLE pizzas_ingredients(
	id_ingredient INT,
	id_pizza INT,
	
	CONSTRAINT fk_pi_ingredient
	FOREIGN KEY (id_ingredient)
	REFERENCES ingredients (numero_ingredient),
	
	CONSTRAINT fk_pi_pizza
	FOREIGN KEY (id_pizza)
	REFERENCES pizzas (numero_pizza),
	
	CONSTRAINT pk_pizzas_ingredients
	PRIMARY KEY (id_ingredient,id_pizza)
);

CREATE TABLE clients
(
	numero_client INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	adresse VARCHAR(255) NOT NULL
);

CREATE TABLE livreurs
( 
	numero_livreur INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(40) NOT NULL,
	telephone VARCHAR(13)NOT NULL
);


CREATE TABLE commandes(
	numero_commande INT PRIMARY KEY AUTO_INCREMENT ,
	heure_commande DATETIME NOT NULL,
	heure_livraison  DATETIME NOT NULL,
	client INT,
	livreur INT,
	
	CONSTRAINT fk_commande_client
	FOREIGN  KEY (client)
	REFERENCES clients(numero_client),
	
	CONSTRAINT fk_commande_livreur
	FOREIGN  KEY (livreur)
	REFERENCES livreurs(numero_livreur)
);

CREATE TABLE pizzas_commandes(
	id_commande INT,
	id_pizza INT,
	quantite TINYINT NOT NULL DEFAULT 1 ,
	
	CONSTRAINT fk_pc_commande
	FOREIGN KEY (id_commande)
	REFERENCES commandes (numero_commande ),
	
	CONSTRAINT fk_pc_pizza
	FOREIGN KEY (id_pizza)
	REFERENCES pizzas (numero_pizza),
	
	CONSTRAINT pizza_commandes
	PRIMARY KEY (id_commande,id_pizza)
);





