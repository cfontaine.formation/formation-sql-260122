USE bibliotheque;

-- Fonctions arithmètiques
SELECT CEIL(1.51),FLOOR(1.51),TRUNCATE(1.333333,2),ROUND(1.51,1),ROUND(1.56,1);

-- Division, Division entière , le reste de la division entière
SELECT 5/2, 5 DIV 2, 5 MOD 2;

-- RAND -> nombre aléatoire entre 0 et 1
SELECT RAND();

-- Fonctions chaines de caractères
-- Valeur en ASCII du caractère a et b
SELECT ascii('a'),ascii('b');

-- Nombre de caractère du nom de chaque auteur
SELECT LENGTH (nom)  AS nb_caractere_titre FROM auteurs;

-- Concaténation du prenom et du nom pour chaque
SELECT concat(prenom, ' ', nom) FROM auteurs; 

-- Concaténation de chaînes avec un séparateur
SELECT concat_ws('-',prenom, nom,id) FROM auteurs; 

SELECT format(1000000045,4);
 
-- Insertion d'une chaîne à une position et pour un certain nombre de caractères
SELECT INSERT(prenom,2,2,nom) FROM auteurs;

-- position de er dans la chaine azerty -> 3 
SELECT POSITION('er' IN 'azerty');

-- Répète 5 fois une chaîne
SELECT REPEAT('er',5);

-- remplace tous les o par un _ dans la chaine hello world
SELECT REPLACE ('hello world','o','_');

-- inversion  de la chaine hello world
SELECT REVERSE ('hello world');

-- 3 premier caractère du prenom et les 3 derniers caractère du prénom des auteurs
SELECT LEFT(prenom,3), RIGHT (prenom,3) FROM auteurs;

-- 
SELECT SPACE(7), STRCMP('bonjour','hello'),STRCMP('hello','bonjour');

SELECT SUBSTR('hello world',7,4);

SELECT TRIM('       azerty uiop        ');

SELECT UPPER('azerty'), LOWER('azerFFVVy');

-- Fonction Date
SELECT now(), current_time(),current_date() ;


SELECT datediff('2023-01-01',current_date());

SELECT date_add(now(),INTERVAL 4 year), date_sub('2021-05-14',INTERVAL 3 DAY);

SELECT date(now());

SELECT DAY(now()),dayname(now()), last_day(now()) , last_day('2022-02-01') ;

SELECT YEAR(now()),MONTH (now()),dayofmonth(now()),dayofweek(now()),dayofyear('2021-05-14');

SELECT weekday('2022-01-24'); 

SELECT SECOND(current_time()), MINUTE (current_time()), HOUR(now());

SELECT timediff('12:00:00',current_time()); 

SELECT time_format(current_time(),'%H %i'); 

SELECT date_format(current_date(),'%d %M %Y') ;

SELECT TIME_TO_SEC(current_time()),SEC_TO_TIME(40000);

-- Fonction d'agrégation
SELECT MAX(annee),MIN(annee) FROM livres;

SELECT MIN(titre) FROM livres;

SELECT MIN(naissance) FROM auteurs;

SELECT TRUNCATE(AVG(annee),0) FROM livres

SELECT COUNT(id) FROM livres;

SELECT COUNT(id) FROM livres WHERE annee BETWEEN 1980 AND 1990;

SELECT COUNT(DISTINCT annee) FROM livres WHERE annee BETWEEN 1950 AND 1980;

SELECT CURRENT_USER() AS utilisateur, DATABASE () AS bdd , version() as version;

SELECT id, bin(id),hex(id),oct(id) FROM livres;

SELECT COALESCE (NULL,3,NULL,6);

SELECT NULLIF(5,"HELLO");

SELECT NULLIF("HELLO","HELLO");

-- Regroupement
SELECT count(id) , nation FROM auteurs  GROUP BY nation

SELECT annee, count(id) FROM livres WHERE annee BETWEEN 1980 AND 1990 GROUP BY annee;

SELECT annee, count(id) AS nb_livre FROM livres WHERE annee BETWEEN 1980 AND 1990 GROUP BY annee HAVING nb_livre>2; 

-- Exercice Fonction 
USE Meteo;

SELECT concat_ws('-',LEFT(nom,3),'0000',LENGTH(nom)) FROM villes;

SELECT max(temp_matin) FROM meteo_data WHERE year(jour)=2010;

SELECT avg(temp_matin) FROM meteo_data WHERE year(jour)=2018 AND ville=2;

SELECT YEAR(jour) AS annee, avg(pression) AS  moyenne_pression FROM meteo_data WHERE ville=1 AND MONTH(jour)=7 GROUP BY annee

SELECT YEAR(jour) AS annee, avg(pression) AS  moyenne_pression FROM meteo_data WHERE ville=1 AND MONTH(jour)=7 GROUP BY annee HAVING moyenne_pression>1017 

