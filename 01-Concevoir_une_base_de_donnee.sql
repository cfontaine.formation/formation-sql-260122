-- Supprimer la base de données exemple si elle existe
DROP DATABASE IF EXISTS exemple;

-- Créer une base de données exemple
CREATE DATABASE exemple;

-- Choisir exemple comme base de données courante 
USE exemple;

-- Créer une table articles qui contient 3 colonnes reference, description et prix
CREATE TABLE articles(
	reference INT PRIMARY KEY,
	description VARCHAR(255) COMMENT 'Description du produit', # COMMENT -> Ajout d'un commentaire sur la colonne
    prix DECIMAL(6,2)
) ENGINE = InnoDB;

-- ENGINE -> Choix du moteur de base de données
-- par défaut InnoDB ->  supporte les transactions et les clés étrangère
-- autre moteur:
--  MEMORY -> stockage en mémoire
-- MyISAM  -> ancien moteur, par défaut pas de support des clés étrangères
--  CSV -> CSV storage engine
-- ...


-- Exercice: Gestion de vols
-- 1 création des tables 
-- 2 Ajout des clés primaires
CREATE TABLE pilotes(
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(60) ,
	nom VARCHAR(60),
	nom_heure_vol INT DEFAULT 100
);

CREATE TABLE vol(
	numero_vol INT PRIMARY KEY AUTO_INCREMENT, # AUTO_INCREMENT
	heure_depart DATETIME NOT NULL,
	ville_depart VARCHAR(255)NOT NULL,
	heure_arrivee DATETIME NOT NULL,
	ville_arrivee VARCHAR(255) NOT NULL
);

CREATE TABLE avions(
	numero_avion INT,
	modele VARCHAR(50) NOT NULL UNIQUE,
	capacite SMALLINT
);

-- Supprimer la table vol
-- DROP TABLE vol;

/* Affichage de la  liste et de la description des tables dans la console */

-- Afficher la liste des tables
SHOW TABLES;

-- Afficher la description de la table vols
DESCRIBE vol;

-- Afficher la description de la table articles avec les comentaires et + de détail
SHOW FULL COLUMNS FROM articles;


/* Modification des tables */

-- Renommer une table
RENAME TABLE vol TO vols;

-- Ajouter une colonne à une table
ALTER TABLE pilotes ADD age TINYINT;

-- Supprimer une colonne à une table
ALTER TABLE pilotes DROP age;

-- Changer le type d'une colonne
ALTER TABLE pilotes MODIFY prenom VARCHAR(50);

-- Renommer une colonne
ALTER TABLE pilotes CHANGE nom_heure_vol nombre_heure_vol INT;

-- Ajout d'une clé primaire sur la table avions (après la création de la table)
ALTER TABLE avions ADD
CONSTRAINT pk_avions PRIMARY KEY (numero_avion);

CREATE TABLE clients(
	numero_client INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(60),
	nom VARCHAR (60),
	adresse VARCHAR(255)
);

-- Relation n-n
-- Table de jointure entre les clients et les articles 
CREATE TABLE clients_articles
(
	id_client INT,
	id_article INT,
	
	CONSTRAINT fk_id_client
	FOREIGN KEY (id_client) 
	REFERENCES clients (numero_client),
	
	CONSTRAINT fk_id_article
	FOREIGN KEY (id_article)
	REFERENCES articles(reference),
	
	CONSTRAINT pk_clients_articles
	PRIMARY KEY (id_client,id_article)
);

-- Relation 1-n
-- ajouter une clé etrangère à une table existante

-- Ajouter une colonne pour la clé etrangère 
ALTER TABLE vols ADD pilotes INT;

-- ajouter la contrainte de clé étrangère
ALTER TABLE vols ADD 
CONSTRAINT fk_pilotes
FOREIGN KEY (pilotes)
REFERENCES pilotes(numero_pilote);

ALTER TABLE vols ADD avions INT;

ALTER TABLE vols ADD
CONSTRAINT fk_avions
FOREIGN KEY (avions)
REFERENCES  avions(numero_avion);


CREATE TABLE hotels(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30),
	adresse VARCHAR(255)
);

CREATE TABLE clients_hotel(
	id_client INT,
	id_hotel INT,
	note TINYINT,
	arriver DATE, 
	
	CONSTRAINT fk_client_h1
	FOREIGN KEY (id_client)
	REFERENCES clients(numero_client),
	
	CONSTRAINT fk_hotel_h1
	FOREIGN KEY (id_hotel)
	REFERENCES hotels (id),
	
	CONSTRAINT pk_clients_hotels
	PRIMARY KEY(id_client,id_hotel)
)



