-- Sélectionner la base des données bibliotheque
USE bibliotheque;

SELECT prenom FROM auteurs;

SELECT prenom,nom FROM auteurs;

-- * -> Obtenir toutes les colonnes d’un tableau
SELECT * FROM auteurs;

-- Quand il y a une ambiguité sur le nom d'une colonne, on ajoute le nom de la table  table.colonne
-- pour différentier les 2 colonnes
SELECT auteurs.nom ,genres.nom genre FROM auteurs,genres;

-- AS permet de spécifier un Alias pour le nom d'un colonne ou d"une table
-- auteur, genre -> Alias de colonne
SELECT auteurs.nom AS auteur ,genres.nom  genre FROM auteurs,genres;

-- ecrivains -> Alias de table
SELECT prenom,nom FROM auteurs AS ecrivains;

-- On peut mettre dans les colonnes d'un SELECT une constante ou une colonne qui provient d'un calcul
SELECT titre, 'age', 2022-annee age FROM livres;

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT DISTINCT prenom FROM auteurs;

-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous le titres de livre qui sont sortie après 1980
SELECT titre, annee FROM livres WHERE annee>1980;

-- Les opérateurs logiques AND et OR permettent de combiner des conditions
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1980 et 1990
SELECT  titre, annee FROM livres WHERE annee>=1980 AND annee<1990;

-- Selection des titres, de l'année de sortie du livre qui sont sorties en 1982 et en 1987
SELECT  titre, annee FROM livres WHERE annee=1982 OR annee=1987;

-- Selection des titres, de l'année de sortie du livre qui sont sorties en 1980 et 1990 ou en 1950 et 1960
SELECT  titre, annee FROM livres WHERE (annee>=1980 AND annee<1990) OR (annee>=1950 AND annee<1960);

-- L'opérateur NOT inverse le resultat
-- Selection des titres, de l'année de sortie du livre qui sont sortie sauf en 1982 (Condition inversée)
SELECT titre, annee FROM livres WHERE NOT (annee=1982)

-- Selection  de tous les livres sortie après 1981 sauf ceux de 1981
SELECT  titre, annee FROM livres WHERE annee=1982 XOR annee>1981;

-- IS NULL permet de tester si une valeur est égal à NULL
-- IS NOT NULL  permet de tester si une valeur est différente de NULL
-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui ne sont pas décédés => deces = NULL 
SELECT prenom, nom FROM auteurs WHERE  deces IS NULL;

-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui ne sont pas décédés
SELECT prenom, nom FROM auteurs WHERE  deces IS NOT NULL;

-- Sélection du titre et de l'année pour les livres sortie en 1954,1965 ou 1978
SELECT  titre, annee FROM livres WHERE annee IN (1964,1992,1987);

-- Sélection du prénom et du nom des auteurs qui sont nés entre le 1er janvier 1950 et le 15 mai 1965
SELECT prenom, nom FROM auteurs WHERE naissance BETWEEN '1950-01-01' AND '1965-05-15';

-- Avec LIKE % représente 0,1 ou plusieurs caratères inconnues
--           _ représente un caratère inconnue
-- Sélection du prénon et du nom de tous les auteurs dont le commence par a (ne tient pas compte de la casse)
SELECT prenom, nom FROM auteurs WHERE nom LIKE 'a%';

-- Sélection du prénon et du nom de tous les auteurs dont le commence par e et qui fait au moins 3 caractères
SELECT prenom, nom FROM auteurs WHERE nom LIKE 'e__%';

-- Sélectionne le titre de tous les livres dont le titre commence par d qui fait 4 caracères
SELECT titre FROM livres WHERE titre LIKE 'd___';

-- Selection de tous les livres triés par rapport à leur titre par ordre alphabétique
SELECT * FROM livres ORDER BY titre;

-- Selection de tous les livres sortie entre 1980 et 1990 triés par rapport à leur année  de sortie (ordre croissant)et à leur titre par ordre alphabétique décroissant
SELECT titre,annee FROM livres WHERE (annee>=1980 AND annee<1990) ORDER BY annee, titre DESC;

-- Sélection des 5 livres les plus récent
SELECT titre ,annee FROM livres ORDER BY annee DESC LIMIT 5;

-- Sélection 3 livres les plus ancien à partir du 10ème
SELECT titre ,annee FROM livres ORDER BY annee  LIMIT 3 OFFSET 10;

-- idem autre syntaxe (MySQL)
SELECT titre ,annee FROM livres ORDER BY annee  LIMIT 10,3;
