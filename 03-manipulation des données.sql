USE exemple;
-- Insertion de données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO articles (1,"processeur",350.0);

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO articles(description,prix)  VALUES ('carte mère',150.0)

-- Insérer plusieurs lignes à la fois
INSERT INTO articles(description,prix) VALUES 
("TV 4k",600.0),
("souris",9.90),
("clavier",19.90),
("smartphone",400.90);
("souris",59.90),
("clavier gamer",99.90),

-- Supprimer des données
-- Supression de la ligne qui a pour id 2 dans la table articles
DELETE FROM articles WHERE id=2;
-- Supression de toutes les lignes qui ont pour description : souris
DELETE FROM articles WHERE description ='souris';

DELETE FROM articles WHERE description ='souris';

-- Supression de toutes les lignes qui ont un prix inférieur à 100.0 dans la table articles 
DELETE FROM articles  WHERE prix <100.0; 

-- Supression de toutes les lignes de la table articles  => ne remet pas les colonnes AUTO_INCREMENT à 0
DELETE FROM articles ;

INSERT INTO articles(description,prix) VALUES 
("TV 4k",600.0),
("souris",9.90),
("clavier",19.90),
("smartphone",400.90);
("souris",59.90),
("clavier gamer",99.90),

SET FOREIGN_KEY_CHECKS = 0;  # désactivation des contraintes de clé étrangères
-- Supression de toutes les lignes de la table pizzas => remet à 0   les colonnes AUTO_INCREMENT
TRUNCATE  articles; 
SET FOREIGN_KEY_CHECKS = 1; # ré-activer les contraintes de clé étrangères

INSERT INTO articles(description,prix) VALUES 
("TV 4k",600.0),
("souris",9.90),
("clavier",19.90),
("smartphone",400.90);
("souris",59.90),
("clavier gamer",99.90),

-- Modification du prix de l'article qui a pour reference 2
UPDATE articles SET prix=100.0 WHERE reference =2;

-- Modification de toutes les lignes de table articles qui ont un prix <100.0 => nouvelle valeur 15.0
UPDATE articles SET prix=15.0 WHERE prix<100.0;

-- Modification de tous les prix augmentation de 10%
UPDATE articles SET prix=prix*1.1; 

USE pizzeria;

-- Modifier des données
-- Modification de la colonne photo pour la ligne qui a pour id 8 => nouvelle valeur pizza.jpg
UPDATE pizzas 
SET photo=LOAD_FILE('C:/Formations/pizza.jpg')
WHERE id=8;

-- Modification de toutes les lignes de table pizzas qui ont un prix <14 => nouvelle valeur 15.0
UPDATE pizzas 
SET prix=15.0
WHERE prix<14.0;

-- Modification de toutes les lignes de table pizzas, On ajoute 2 aux prix de toutes les pizzas 
UPDATE pizzas 
SET prix=prix+2;

INSERT INTO pizzas(nom, photo) VALUES ("pizza classique");

-- LOAD_FILE permet de charger l'image pizza.jpg dans photo qui de type BLOB (données binaires)
UPDATE pizzas 
SET photo=LOAD_FILE('C:/Dawan/Formations/Sql260122/pizza.jpg')
WHERE id=1;