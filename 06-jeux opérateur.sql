USE exemple;

CREATE TABLE employes(
	id int PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(60),
	nom VARCHAR(60),
	adresse VARCHAR(255),
	salaire DOUBLE,
	arriver DATE
);

INSERT INTO employes VALUES (1,'joe','dalton','rue national Lille',2000.0,'2001-04-01' ),
 (2,'Allan','Smithee','nantes',2300.0,'2008-01-01' ),
  (3,'marcel','durand','Paris',3000.0,'2000-06-01' );
 INSERT INTO clients VALUES (4,'joe','dalton','rue national Lille');

INSERT INTO employes VALUES  (4,'Allan','Smithee','nantes',2300.0,'2008-01-01' ),
 

SELECT prenom, nom, adresse FROM employes
 UNION
 SELECT prenom, nom, adresse FROM clients;

 SELECT prenom, nom, adresse FROM employes
 UNION ALL
 SELECT prenom, nom, adresse FROM clients;

SELECT DISTINCT prenom, nom, adresse FROM clients 
WHERE prenom IN (SELECT prenom FROM employes) AND nom IN (SELECT  nom FROM employes) AND adresse IN (SELECT adresse FROM employes);
 

